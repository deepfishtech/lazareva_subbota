$('#menuOpen').click(function(){
	$('#menuOpen').toggleClass('active');
	$('.nav-menu').toggleClass('show');
});

$(window).on('scroll', function(){
  if($(window).scrollTop() > 300){
    if($('.toTop').hasClass('show') != true){
      $('.toTop').addClass('show');
    } 
  } else {
    if($('.toTop').hasClass('show') == true){
      $('.toTop').removeClass('show');
    } 
  }
});

$('.toTop').on('click', function(){
  $('body, html').animate({
    scrollTop: 0,
  }, 500);
});

if( $('.partners-carousel__list').length > 0 ){
// 
  $('.partners-carousel__list').slick({
  	infinite: true,
  	slidesToShow: 7,
  	slidesToScroll: 1,
  	prevArrow: $('.partners-carousel__arrow-left'),
      nextArrow: $('.partners-carousel__arrow-right'),
      responsive: [
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 4
          }
        },
      ]
  });
// 
}
if( $('.post-photos__carousel').length > 0 ){
//
  $('.post-photos__carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $('.photo-carousel__prev'),
    nextArrow: $('.photo-carousel__next'),
    responsive: [
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 721,
        settings: {
          slidesToShow: 1
        }
      },
    ]
  });
// 
}




$(document).ready(function() {
  if($('#photosList').length > 0){
  // 
    $('#photosList').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1], // Will preload 0 - before current, and 1 after the current image
        tCounter: 'Фото %curr% из %total%'
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        markup: 
          '<div class="mfp-figure">'+
            '<button class="gallery-prev" id="galleryPrev" onclick="prevItem()"></button>'+
            '<button class="gallery-next" id="galleryNext" onclick="nextItem()"></button>'+
            // '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
            // '<div class="mfp-title"></div>'+
            '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>',
      },
    });
  // 
  }


  if($('#videoList') > 0){
  // 
    $('#videoList').magnificPopup({
      delegate: 'a',
      type: 'iframe',
      iframe: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        markup: '<div class="mfp-iframe-scaler">'+
                  '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div>',

         patterns: {
          youtube: {
            index: 'youtube.com/',

            id: 'v=',

            src: 'https://www.youtube.com/embed/%id%?autoplay=1' 
          },

        },
      },
    });
  // 
  }


});

  function nextItem(){
    // console.log('Das');
    $('#photosList').magnificPopup('next');
  }
  function prevItem(){
    // console.log('Das');
    $('#photosList').magnificPopup('prev');
  }


$('#acceptConditions').on('click', function(){
  $("#oferta-checkbox").prop('checked','checked');
});